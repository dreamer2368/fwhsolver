#!/usr/bin/env python
# -*- coding: utf-8 -*-

import plot3dnasa as p3d
import numpy as np

def get_fromfile(offset, size, n, probe_files):
    q = np.empty([n[1], n[0], 5, size], order='F')
    m = (n[1] - 1) / 4 + 1
    n = 8 * m * n[0] * 5 * size
    for i, filename in enumerate(probe_files):
        with open(filename) as f:
            f.seek(offset * n / size)
            data_buffer = np.fromstring(f.read(n),dtype='<f8')
            chunk_size = data_buffer.size/(m*q.shape[1]*5)
            q[i*(m-1):(i+1)*(m-1),:,:,:] = np.reshape(
                data_buffer, [m, q.shape[1], 5, chunk_size],
                order='F')[:-1,:,:,:]
    return q #(129,512,5,size)

def extract_P_const_r_fromQfile(radial_idx, Qfile, gamma=1.4):
    s = p3d.Solution().load(Qfile)
    Qshape = s.q[1].shape                            #(256,33,512,5)
    q = np.empty([4*(Qshape[1]-1)+1, Qshape[2], 5], order='F')
    m = Qshape[1]
    for i in range(1,5):
        q[(i-1)*(m-1):i*(m-1),:,:] = s.q[i][radial_idx,:-1,:,:]
    q[-1,:,:] = q[0,:,:]
    q[:,:,0] = 1. / q[:,:,0]
    q[:,:,4] = (gamma - 1.) * (
                        q[:,:,4] - 0.5 * q[:,:,0] * np.sum(
                        q[:,:,i+1]**2 for i in range(3)) )
    return q[:,:,4] #(129,512)

class FWHSolver:
    def __init__(self, g, mikes, nsamples, dt, probe_files=None, meanP=None, gamma=1.4):
        d_min = min((mike.min_dist(g.xyz[0]) for mike in mikes))
        d_max = max((mike.max_dist(g.xyz[0]) for mike in mikes))
        self.nsamples = nsamples
        nsteps = nsamples - (int(np.ceil(d_max / dt)) -
                             int(np.floor(d_min / dt)))
#        assert nsteps > 0
        self.dt = dt
        self.meanP = meanP
        self.mikes = mikes
        self.gamma = gamma
        cell_areas, unit_normals = self._compute_normals(g.xyz[0])
        for mike in mikes:
            mike.set_params(np.rollaxis(g.xyz[0][:,:,0,:], axis=1, start=0),
                            cell_areas, unit_normals, dt, nsteps,
                            nsamples, int(np.ceil(d_max / dt)))
        self.get = get_fromfile
        self.get_args = (g.get_size(0), probe_files)

    def integrate(self, chunk_size=20):
        pbar = None
        try:
            from progressbar import ProgressBar, Percentage, Bar, ETA
            print 'Processing FWH data...'
            pbar = ProgressBar(widgets = [Percentage(), ' ', Bar(
                '=', left = '[', right = ']'), ' ', ETA()],
                               maxval = self.nsamples).start()
        except ImportError:
            pass
        meanP = np.tile(self.meanP,(chunk_size,1,1))
        meanP = np.swapaxes( np.swapaxes(meanP,0,1), 1,2 )
        for i in range(self.nsamples):
            if i % chunk_size == 0:
                q = self.get(i, chunk_size, *self.get_args)
                q[-1,:,:,:] = q[0,:,:,:]
                q[:,:,0,:] = 1. / q[:,:,0,:]
                q[:,:,4,:] = (self.gamma - 1.) * (
                    q[:,:,4,:] - 0.5 * q[:,:,0,:] * np.sum(
                        q[:,:,i+1,:]**2 for i in range(3))) - meanP
            for mike in self.mikes:
                mike.add_contribution(i, q[:,:,:,i%chunk_size])
            if pbar:
                pbar.update(i)
            if i % 100 == 0:
                for j, mike in enumerate(self.mikes):
                    with open('mike%02d.dat' % (j + 1), 'w') as f:
                        np.savetxt(f, np.array([mike.t, mike.p]).T, fmt='%+.18E')
        if pbar:
            pbar.finish()

    def _compute_normals(self, xyz):
        """Computes the areas of quadrilateral elements and the unit
        normal vector at each cell."""
        a = np.cross(xyz[1:,:-1,0,:] - xyz[:-1,:-1,0,:],
                     xyz[:-1,1:,0,:] - xyz[:-1,:-1,0,:])
        b = np.cross(xyz[:-1,1:,0,:] - xyz[1:,1:,0,:],
                     xyz[1:,:-1,0,:] - xyz[1:,1:,0,:])
        ab = np.sqrt(np.sum((a + b) ** 2, axis=-1))
        cell_areas = 0.5 * (np.sqrt(np.sum(a ** 2, axis=-1)) + 
                            np.sqrt(np.sum(b ** 2, axis=-1)))
        unit_normals = a + b
        for i in range(unit_normals.shape[-1]):
            unit_normals[:,:,i] /= ab
        return np.asfortranarray(cell_areas.T), np.asfortranarray(
            np.rollaxis(unit_normals, axis=1, start=0))

    def spatial_average(self, chunk_size=20):
        pbar = None
        try:
            from progressbar import ProgressBar, Percentage, Bar, ETA
            print 'Processing FWH data...'
            pbar = ProgressBar(widgets = [Percentage(), ' ', Bar(
                '=', left = '[', right = ']'), ' ', ETA()],
                               maxval = self.nsamples).start()
        except ImportError:
            pass
        niter, remainder = divmod(self.nsamples,chunk_size)
        time = np.array([i*self.dt for i in range(self.nsamples)])
        pressure = np.zeros(self.nsamples)
        for i in range(niter):
            sample_step = i * chunk_size
            q = self.get(sample_step, chunk_size, *self.get_args)
            q[-1,:,:,:] = q[0,:,:,:]
            # rho inverse
            q[:,:,0,:] = 1. / q[:,:,0,:]
            # pressure fluctuation
            q[:,:,4,:] = (self.gamma - 1.) * (
                q[:,:,4,:] - 0.5 * q[:,:,0,:] * np.sum(
                    q[:,:,i+1,:]**2 for i in range(3)))
            pressure[sample_step:sample_step+chunk_size] = np.mean( np.mean( q[:,:,4,:], axis=0 ), axis=0 )
            if pbar:
                pbar.update(i)
        with open('spatial_average.dat', 'w') as f:
            np.savetxt(f, np.array([time, pressure]).T, fmt='%+.18E')
        if pbar:
            pbar.finish()

def windowed_fft(p, num_windows=5, dt=0.18, window_type='blackman'):
    import numpy.fft
    from scipy.signal import get_window
    n = p.shape[0]
    m = 2 * (n / (num_windows + 1))
    windows = [((int(0.5 * i * m), int(0.5 * i * m) + m))
               for i in range(num_windows)]
    y = np.empty([(m + 1) // 2, num_windows, p.shape[1]])
    if window_type:
        window_func = get_window(window_type, m)
    else:
        window_func = np.ones(m)
    for j in range(p.shape[1]):
        for i, w in enumerate(windows):
            y[:,i,j] = np.absolute(numpy.fft.fft(
                p[w[0]:w[1],j] * window_func))[:(m+1)//2] / window_func.sum()
            y[1:,i,j] *= np.sqrt(2.)
    p_hat = np.sqrt(np.mean(np.mean(y ** 2, axis=1), axis=1))
    return numpy.fft.fftfreq(m, d=dt)[:p_hat.size], p_hat

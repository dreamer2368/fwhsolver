import numpy as np
import plot3dnasa as p3d

class Mike:
    def __init__(self, xyz=[0., 0., 0.]):
        self.xyz = xyz
        #These slices denotes 4 corners of every cell on FWH surface.
        #Cell value used for surface integral is evaluated as
        # arithemtic average of point values at 4 corner.
        self.slices = [[slice(None, -1), slice(None, -1)],
                       [slice(None, -1), slice(+1, None)],
                       [slice(+1, None), slice(None, -1)],
                       [slice(+1, None), slice(+1, None)]]
        self.coeff = p3d.fdcoeff([-2, -1, 0, 1, 2], order=1)

    def min_dist(self, xyz):
        """Distance to the closest grid point."""
        return np.sqrt(np.sum((xyz - self.xyz) ** 2, axis=-1)).min()

    def max_dist(self, xyz):
        """Distance to the farthest grid point."""
        return np.sqrt(np.sum((xyz - self.xyz) ** 2, axis=-1)).max()

    def set_params(self, xyz, cell_areas, unit_normals, dt, nsteps,
                   nsamples, offset):
        n = np.array(xyz.shape[0:2], 'int64') - 1
        self._allocate(n, nsteps)
        dist = np.empty_like(self.normal_projection)
        for i, s in enumerate(self.slices):
            self.disp[:,:,:,i] = self.xyz - xyz[s + [slice(None)]]
            dist[:,:,i] = np.sqrt(np.sum(self.disp[:,:,:,i] ** 2, axis=-1))
        self.dist_inverse = 1. / dist
        #time index offset due to radiation time
        self.advanced_offset = dist / dt - self.coeff.size // 2
        for i in range(3):
            self.disp[:,:,i,:] = self.disp[:,:,i,:] * self.dist_inverse
        self.cell_areas = cell_areas
        self.unit_normals = unit_normals
        for i in range(len(self.slices)):
            self.normal_projection[:,:,i] = np.sum(
                self.unit_normals * self.disp[:,:,:,i], axis=-1)
        self.signal_offset = offset
        # len(self.slices) indicates
        # arithmetic average of 4 point-values in a cell
        self.dp_factor = 1. / (4. * np.pi * len(self.slices))
        self.coeff /= dt
        #Interpolation weight in time index for accurate radiation offset
        self.weights = self.advanced_offset - np.trunc(self.advanced_offset)
        self.t = (self.signal_offset + 1 + np.arange(nsteps)) * dt
        return self

    def _allocate(self, n, nsteps):
        self.disp = np.empty([n[0], n[1], 3, len(self.slices)], order='F')
        self.normal_projection = np.empty([n[0], n[1], len(self.slices)],
                                          order='F')
        self.Q = np.empty([n[0], n[1], len(self.slices), self.coeff.size],
                          order='F') # monopole strength
        self.L = np.empty_like(self.Q) # dipole strength
        self.p = np.zeros(nsteps)

    def add_contribution(self, sample_index, q):
        #update one step
        self.Q[:,:,:,:-1] = self.Q[:,:,:,1:]
        self.L[:,:,:,:-1] = self.L[:,:,:,1:]
        for i, s in enumerate(self.slices):
            #monopole: rho*U*normal
            self.Q[:,:,i,-1] = np.sum(q[s + [j+1]] * self.unit_normals[:,:,j]
                                      for j in range(3))
            #dipole: P*normal*radiation + rho*U*U*normal*radiation
            self.L[:,:,i,-1] = self.normal_projection[:,:,i] * q[s + [4]] + \
                               q[s + [0]] * self.Q[:,:,i,-1] * \
                np.sum(q[s + [j+1]] * self.disp[:,:,j,i] for j in range(3))
        if sample_index < self.coeff.size - 1:
            return self
        # \dot{Q}/r + \dot{L}/r + L/r^2
        dp = (np.sum((self.L[:,:,:,i] + self.Q[:,:,:,i]) * c
                     for i, c in enumerate(self.coeff)) +
              self.L[:,:,:,self.coeff.size//2] * self.dist_inverse) * \
            self.dist_inverse * self.dp_factor
        # Surface integral
        for i in range(dp.shape[-1]):
            dp[:,:,i] *= self.cell_areas
        return self._update_signal(sample_index, dp)

    def _update_signal(self, sample_index, dp):
        # compute radiation time, and accumulate signals in time
        a = np.trunc(self.advanced_offset + sample_index) - \
            self.signal_offset - 1
        # use interpolation in time
        self.p[:-1] += np.histogram(a, range(self.p.size),
                                    (0., self.p.size - 2.),
                                    weights=(1. - self.weights) * dp)[0]
        self.p[1:] += np.histogram(a, range(self.p.size),
                                   (0., self.p.size - 2.),
                                   weights=self.weights * dp)[0]
        return self